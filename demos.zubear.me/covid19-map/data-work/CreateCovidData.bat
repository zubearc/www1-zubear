: Update git repos
call update.bat
: Update global JSONs
python CreateGlobalCovidData.py
: Update USA data
python fixNYC.py
python3 CreateUSACovidData.py
cd nyc-coronavirus-data
python maketimeline.py
copy dod-zip.json ..\..\data\usa\covid19-nyc-zt.json
cd ..