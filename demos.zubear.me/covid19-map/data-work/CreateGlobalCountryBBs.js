
var fs = require('fs');
var countries = JSON.parse(fs.readFileSync('./global/countries.geojson', 'utf8'));

var boxesForCountries = {};

var madeBBs = 0;

var iso2toiso3 = {'DZA': 'DZ', 'AGO': 'AO', 'EGY': 'EG', 'BGD': 'BD', '': '', 'LIE': 'LI', 'FSM': 'FM', 'NAM': 'NA', 'BGR': 'BG', 'BOL': 'BO', 'GHA': 'GH', 'CCK': 'CC', 'PAK': 'PK', 'CPV': 'CV', 'JOR': 'JO', 'LBR': 'LR', 'LBY': 'LY', 'MYS': 'MY', 'ATA': 'AQ', 'PRI': 'PR', 'MYT': 'YT', 'PRK': 'KP', 'PSE': 'PS', 'TZA': 'TZ', 'PRT': 'PT', 'KHM': 'KH', 'UMI': 'UM', 'PRY': 'PY', 'HKG': 'HK', 'SAU': 'SA', 'LBN': 'LB', 'SVN': 'SI', 'BFA': 'BF', 'SVK': 'SK', 'MRT': 'MR', 'HRV': 'HR', 'CHL': 'CL', 'CHN': 'CN', 'KNA': 'KN', 'JAM': 'JM', 'GIB': 'GI', 'DJI': 'DJ', 'GIN': 'GN', 'FIN': 'FI', 'URY': 'UY', 'VAT': 'VA', 'STP': 'ST', 'SYC': 'SC', 'NPL': 'NP', 'CXR': 'CX', 'LAO': 'LA', 'YEM': 'YE', 'LSO': 'LS', 'PHL': 'PH', 'ZAF': 'ZA', 'NIC': 'NI', 'BVT': 'BV', 'SXM': 'SX', 'ROU': 'RO', 'VIR': 'VI', 'SYR': 'SY', 'MAC': 'MO', 'MAF': 'MF', 'MLT': 'MT', 'KAZ': 'KZ', 'COK': 'CK', 'TCA': 'TC', 'PYF': 'PF', 'SUR': 'SR', 'CUW': 'CW', 'DMA': 'DM', 'BEN': 'BJ', 'NGA': 'NG', 'BEL': 'BE', 'SGS': 'GS', 'MSR': 'MS', 'TGO': 'TG', 'DEU': 'DE', 'GUM': 'GU', 'LKA': 'LK', 'SSD': 'SS', 'FLK': 'FK', 'GBR': 'GB', 'BES': 'BQ', 'GUY': 'GY', 'CRI': 'CR', 'CMR': 'CM', 'PCN': 'PN', 'MAR': 'MA', 'MNP': 'MP', 'COM': 'KM', 'HUN': 'HU', 'TKM': 'TM', 'TTO': 'TT', 'NLD': 'NL', 'BMU': 'BM', 'HMD': 'HM', 'TCD': 'TD', 'GEO': 'GE', 'MNE': 'ME', 'MNG': 'MN', 'MHL': 'MH', 'MTQ': 'MQ', 'BLZ': 'BZ', 'NFK': 'NF', 'AFG': 'AF', 'BDI': 'BI', 'VGB': 'VG', 'BLR': 'BY', 'BLM': 'BL', 'GRD': 'GD', 'ALA': 'AX', 'TKL': 'TK', 'GRC': 'GR', 'XKS': 'XK', 'GRL': 'GL', 'SHN': 'SH', 'AND': 'AD', 'MOZ': 'MZ', 'NIU': 'NU', 'THA': 'TH', 'HTI': 'HT', 'MEX': 'MX', 'ZWE': 'ZW', 'LCA': 'LC', 'IND': 'IN', 'LVA': 'LV', 'TJK': 'TJ', 'BTN': 'BT', 'VCT': 'VC', 'VNM': 'VN', 'NOR': 'NO', 'CZE': 'CZ', 'ATF': 'TF', 'ATG': 'AG', 'FJI': 'FJ', 'IOT': 'IO', 'HND': 'HN', 'MUS': 'MU', 'DOM': 'DO', 'LUX': 'LU', 'ISR': 'IL', 'SMR': 'SM', 'PER': 'PE', 'REU': 'RE', 'IDN': 'ID', 'VUT': 'VU', 'MKD': 'MK', 'COD': 'CD', 'COG': 'CG', 'ISL': 'IS', 'GLP': 'GP', 'ETH': 'ET', 'NER': 'NE', 'COL': 'CO', 'GUF': 'GF', 'BWA': 'BW', 'NRU': 'NR', 'MDA': 'MD', 'GGY': 'GG', 'MDG': 'MG', 'ECU': 'EC', 'KIR': 'KI', 'SEN': 'SN', 'ESH': 'EH', 'MDV': 'MV', 'ASM': 'AS', 'SPM': 'PM', 'SRB': 'RS', 'FRA': 'FR', 'LTU': 'LT', 'RWA': 'RW', 'ZMB': 'ZM', 'GMB': 'GM', 'WLF': 'WF', 'JEY': 'JE', 'FRO': 'FO', 'GTM': 'GT', 'DNK': 'DK', 'UKR': 'UA', 'AUS': 'AU', 'AUT': 'AT', 'SJM': 'SJ', 'VEN': 'VE', 'PLW': 'PW', 'KEN': 'KE', 'WSM': 'WS', 'TUR': 'TR', 'ALB': 'AL', 'OMN': 'OM', 'TUV': 'TV', 'MMR': 'MM', 'BRN': 'BN', 'TUN': 'TN', 'RUS': 'RU', 'BRB': 'BB', 'BRA': 'BR', 'CAN': 'CA', 'TLS': 'TL', 'GNQ': 'GQ', 'USA': 'US', 'QAT': 'QA', 'SWE': 'SE', 'AZE': 'AZ', 'GNB': 'GW', 'SWZ': 'SZ', 'TON': 'TO', 'CIV': 'CI', 'KOR': 'KR', 'AIA': 'AI', 'CAF': 'CF', 'CHE': 'CH', 'CYP': 'CY', 'BIH': 'BA', 'SGP': 'SG', 'TWN': 'TW', 'SOM': 'SO', 'UZB': 'UZ', 'ERI': 'ER', 'POL': 'PL', 'KWT': 'KW', 'GAB': 'GA', 'CYM': 'KY', 'EST': 'EE', 'MWI': 'MW', 'ESP': 'ES', 'IRQ': 'IQ', 'SLV': 'SV', 'MLI': 'ML', 'IRL': 'IE', 'IRN': 'IR', 'ABW': 'AW', 'SLE': 'SL', 'IMN': 'IM', 'PAN': 'PA', 'SDN': 'SD', 'SLB': 'SB', 'NZL': 'NZ', 'MCO': 'MC', 'ITA': 'IT', 'JPN': 'JP', 'KGZ': 'KG', 'UGA': 'UG', 'NCL': 'NC', 'ARE': 'AE', 'ARG': 'AR', 'BHS': 'BS', 'BHR': 'BH', 'ARM': 'AM', 'PNG': 'PG', 'CUB': 'CU'};

function makeBounds(points, code) {
    // https://gis.stackexchange.com/a/172561
    let lats = []; let lngs = [];

    for (var i = 0; i < points.length; i++) {
        lats.push(points[i][1]);
        lngs.push(points[i][0]);
        // following not needed to calc bbox, just so you can see the points
        // L.marker([points[0][i][1], points[0][i][0]]).addTo(map);
    }

    // calc the min and max lng and lat
    var minlat = Math.min.apply(null, lats),
        maxlat = Math.max.apply(null, lats);
    var minlng = Math.min.apply(null, lngs),
        maxlng = Math.max.apply(null, lngs);

    // create a bounding rectangle that can be used in leaflet
    let bbox = [[parseFloat(minlat.toFixed(4)), parseFloat(minlng.toFixed(4))], [parseFloat(maxlat.toFixed(4)), parseFloat(maxlng.toFixed(4))]];
    // end snip

    // console.log(lats, lngs, bbox);
    if (!minlat) {
        console.log(points, lats, lngs, bbox);
    }

    if (boxesForCountries[code]) {
        boxesForCountries[code].push(bbox);
    } else {
        boxesForCountries[code] = [bbox];
    }

    madeBBs++;
}

for (var feature of countries.features) {
    let props = feature['properties'];
    let _code = props['ISO_A3'];
    let code = iso2toiso3[_code];

    var coords = feature['geometry']['coordinates'];

    if (feature['geometry']['type'] == 'MultiPolygon') {
        for (var collection of coords) {
            epolysc = []
            for (pairs of collection) {
                // epolys.append(Polylines.encode_coords(pairs));

                makeBounds(pairs, code);
            }
        }
    } else {
        for (var pairs of coords) {
            // epolys.append(Polylines.encode_coords(pairs))
            makeBounds(pairs, code);
        }
    }
}


// console.log(boxesForCountries);
console.log('made', madeBBs, 'bbs');

fs.writeFileSync('../data/global/country-bounds.json', JSON.stringify(boxesForCountries));