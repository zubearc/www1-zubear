import json

with open('states-polygons.json', 'r') as f:
    states_polygons = json.loads(f.read())

with open('states.json', 'r') as f:
    states_fc = json.loads(f.read())

with open('usa-state-bbs.csv', 'r') as f:
    states_bbs_lines = f.readlines()
    states_bbs_lines.pop(0)

states_bbs = {}

for line in states_bbs_lines:
    line = line.replace('"', '').strip()
    _, statefips, usps, name, xMin, yMin, xMax, yMax = line.split(",")
    states_bbs[statefips] = [[float(yMin), float(xMin)], [float(yMax), float(xMax)]]

out = {
    'bbs': states_bbs,
    'fcs': states_fc,
    'polys': states_polygons
}

with open('../data/usa/states.json', 'w') as f:
    f.write(json.dumps(out))