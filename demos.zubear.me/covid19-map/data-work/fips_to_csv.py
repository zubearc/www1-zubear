import json
import Polylines

file = 'sample_usa_counties.json'
file = 'I:/Development/Projects/Covid19/usa-counties/usa-counties-5.geojson'

with open(file) as f:
    j = json.loads(f.read())

# 

CHANGES = {
    '36005': [ 'The Bronx - NYC' ],
    '36081': [ 'Queens - NYC' ],
    '36047': [ 'Brooklyn - NYC' ],
    '36061': [ 'Manhattan - NYC' ],
    '36085': [ 'Staten Island - NYC' ]
}

#

out = {}
state_map = {}
county_data_map = {}

for feature in j['features']:
    props = feature['properties']
    coordpairs = feature['geometry']['coordinates']
    fips = props['FIPS']
    state = props['STATE_FIPS']
    population2012 = props['POP2012']

    name = props['NAME']
    males = props['MALES']
    females = props['FEMALES']
    ages_1to19 = int(props['AGE_UNDER5']) + int(props['AGE_5_9']) + int(props['AGE_10_14']) + int(props['AGE_15_19'])
    ages_20to34 = int(props['AGE_20_24']) + int(props['AGE_25_34'])
    ages_35to64 = int(props['AGE_35_44']) + int(props['AGE_45_54']) + int(props['AGE_55_64'])
    ages_65plus = int(props['AGE_65_74']) + int(props['AGE_75_84']) + int(props['AGE_85_UP'])

    if fips in CHANGES:
        name = CHANGES[fips][0]

    epolys = []
    for collection in coordpairs:
        epolysc = []
        for pairs in collection:
            # print pairs
            # print encode_coords(pairs)
            fp = pairs[0]
            lp = pairs[1]
            simplifiable = pairs[1:]
            simplified = Polylines.simplify(simplifiable, 0.001)
            new = []
            new.append(fp)
            new += simplified
            new.append(lp)
            # print pairs, '->' 
            # print new
            # exit()
            epolys.append(Polylines.encode_coords(simplified))
        # epolys.append(epolysc)
    # exit()
    # print epolys
    out[fips] = { 'polys': epolys, 'pop12': population2012, 'n': name, 'm': int(males), 'f': int(females), '1_19': ages_1to19, '20_34': ages_20to34, '35_65': ages_35to64, '65': ages_65plus }
    if state in state_map:
        state_map[state].append(fips)
    else:
        state_map[state] = [fips]

# print json.dumps(out)

with open('counties.json', 'w') as f:
    f.write(json.dumps(out))

with open('states.json', 'w') as f:
    f.write(json.dumps(state_map))