import urllib2

days = """03-18-2020
03-19-2020
03-20-2020
03-21-2020
03-22-2020
03-23-2020
03-24-2020
03-25-2020
03-26-2020
03-27-2020
03-28-2020
03-28-2020
03-29-2020
03-30-2020
03-31-2020
04-01-2020
04-02-2020
04-03-2020
04-04-2020
04-05-2020
04-06-2020
04-07-2020
04-08-2020
04-09-2020
04-10-2020
04-11-2020
04-12-2020
04-13-2020
04-14-2020
04-15-2020
04-16-2020
04-17-2020
04-18-2020
04-19-2020
04-20-2020
04-21-2020
04-22-2020
04-23-2020
04-24-2020
04-25-2020
04-26-2020
04-27-2020
04-28-2020
04-28-2020
04-29-2020
04-30-2020
04-31-2020""".split('\n')

# 2020-04-01,Ontario,New York,36069,24,0

newLines = []

def fetch_cases():
    url = 'https://raw.githubusercontent.com/chazeon/NYState-COVID-19-Tracker/master/data/NYC-covid-19-daily-data-summary.csv'
    r = urllib2.urlopen(url)
    return r.read()


def fetch_deaths():
    url = 'https://raw.githubusercontent.com/chazeon/NYState-COVID-19-Tracker/master/data/NYC-covid-19-daily-data-summary-deaths.csv'
    r = urllib2.urlopen(url)
    return r.read()


# PEOPLE WHO FORMAT CSVs LIKE THIS ARE FUCKING MORONS!! VARIABLE LENGTH DATA IN ROWS, NOT COLUMNS !!!!!!!!!!!!!!!

forDay = {}

def work2(csvdata):
    new = []
    rows = []
    cl = 0
    for line in csvdata.strip().split('\n'):
        s = line.strip().split(',')
        cl = len(s)
        rows.append(s)
    
    for x in range(0, cl):
        ng = ''
        for y in range(0, len(rows)):
            ng += rows[y][x] + ','
            # print rows[y][x]
        print ng

        new.append(ng.split(','))


    for row in new:
        print row
        date = row[0].split(' ')[0]
        if '-' not in date:
            continue
        if date not in forDay:
            forDay[date] = {}

        # BX
        cases = row[13]
        # print 'bx',cases

        forDay[date]['BX'] = [cases, '0']
        # BK
        cases = row[14]

        forDay[date]['BK'] = [cases, '0']

        # MN
        cases = row[15]

        forDay[date]['MN'] = [cases, '0']

        # QN
        cases = row[16]

        forDay[date]['QN'] = [cases, '0']


        # SI
        cases = row[17]

        forDay[date]['SI'] = [cases, '0']



def work2_deaths(csvdata):
    new = []
    rows = []
    cl = 0
    for line in csvdata.strip().split('\n'):
        s = line.split(',')
        cl = len(s)
        rows.append(s)
    
    for x in range(0, cl):
        ng = ''
        for y in range(0, len(rows)):
            ng += rows[y][x] + ','
            # print rows[y][x]
        print ng

        new.append(ng.split(','))


    for row in new:
        print row
        date = row[0].split(' ')[0]
        if '-' not in date:
            continue
        print date
        if date not in forDay:
            forDay[date] = { 'BX': ['0','0'],'BK': ['0','0'],'MN': ['0','0'],'QN': ['0','0'],'SI': ['0','0'] }

        # BX
        cases = row[8]

        forDay[date]['BX'][1] = cases
        # BK
        cases = row[9]

        forDay[date]['BK'][1] = cases

        # MN
        cases = row[10]

        forDay[date]['MN'][1] = cases

        # QN
        cases = row[11]

        forDay[date]['QN'][1] = cases


        # SI
        cases = row[12]
        # print 'SI',cases

        forDay[date]['SI'][1] = cases


        # print row[13]

# def work(csvdata):
#     print csvdata

#     dayMap = []
#     bx = []
#     bk = []
#     mn = []
#     qn = []
#     si = []

#     for line in csvdata.split('\n'):
#         s = line.strip().split(',')
#         # print s
#         if len(s) < 2:
#             continue

#         if s[1] == 'Subgroup':
#             for i in range(2, len(s) - 1):
#                 dayMap.append(s[i])
#             pass

#         s = line.split(',')

#         if s[1] == 'Bronx':
#             for i in range(2, len(s) - 1):
#                 bx.append(s[i])
#             pass
#         if s[1] == 'Brooklyn':
#             for i in range(2, len(s) - 1):
#                 bk.append(s[i])
#             pass
#         if s[1] == 'Manhattan':
#             for i in range(2, len(s) - 1):
#                 mn.append(s[i])
#             pass
#         if s[1] == 'Queens':
#             for i in range(2, len(s) - 1):
#                 qn.append(s[i])
#             pass
#         if s[1] == 'Staten Island':
#             for i in range(2, len(s) - 1):
#                 si.append(s[i])
#             pass

#     print bx, bk, mn, qn, si

#     with open('covid-19-data/us-counties.csv', 'r') as f:
#         nyt_county_data = f.read()
#         # nyt_county_data.pop(0)

#     return [dayMap, bx, bk, mn, qn, si]

cases = fetch_cases()
deaths = fetch_deaths()

# pCases = work(cases)
# pDeaths = work(deaths)

# for day in days:
#     day, month, year = day.split('-')
#     for p in pCases[0]:
#         df = year + '-' + month + '-' + day
#         if df in p[0]
    
# for k,v in enumerate(bx):
#     d = days[k]
#     newLines.append("%s,Bronx,New York,36005,")



print work2(cases.replace('\r', ''))
print work2_deaths(deaths.replace('\r', ''))
print forDay

out = []

for day in forDay:
    e = forDay[day]
    out.append([day, 'Bronx', 'New York', '36005', e['BX'][0], e['BX'][1]])
    out.append([day, 'Brooklyn', 'New York', '36047', e['BK'][0], e['BK'][1]])
    out.append([day, 'Manhattan', 'New York', '36061', e['MN'][0], e['MN'][1]])
    out.append([day, 'Queens', 'New York', '36081', e['QN'][0], e['QN'][1]])
    out.append([day, 'Staten Island', 'New York', '36085', e['SI'][0], e['SI'][1]])

s = ''
for o in out:
    # print o
    s += ','.join(o) + '\n'

with open('USA-Counties-NYT-Extra.csv', 'w') as f:
    f.write(s)