import json
import Polylines

with open('usa/us-states-gons.geojson', 'r') as f:
    j = json.loads(f.read())

out = {}

for feature in j['features']:
    props = feature['properties']
    code = props['code_local']
    coords = feature['geometry']['coordinates']

    print props['name'], code

    fipsc = code.replace('US', '')

    epolys = []
    if feature['geometry']['type'] == 'MultiPolygon':
        for collection in coords:
            epolysc = []
            for pairs in collection:
                # print pairs
                # print encode_coords(pairs)
                epolys.append(Polylines.encode_coords(pairs))
            # epolys.append(epolysc)
    else:
        for pairs in coords:
            # print pairs
            # print encode_coords(pairs)
            epolys.append(Polylines.encode_coords(pairs))
    print epolys

    out[fipsc] = epolys

with open('states-polygons.json', 'w') as f:
    f.write(json.dumps(out))