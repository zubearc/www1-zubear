var campuses = {
    "BAR01": "Baruch College",
    "BMC01": "Borough of Manhattan CC",
    "BCC01": "Bronx CC",
    "BKL01": "Brooklyn College",
    "CTY01": "City College",
    "CSI01": "College of Staten Island",
    "GRD01": "Graduate Center",
    "NCC01": "Guttman CC",
    "HOS01": "Hostos CC",
    "HTR01": "Hunter College",
    "JJC01": "John Jay College",
    "KCC01": "Kingsborough CC",
    "LAG01": "LaGuardia CC",
    "LEH01": "Lehman College",
    "MHC01": "Macaulay Honors College",
    "MEC01": "Medgar Evers College",
    "NYT01": "NYC College of Technology",
    "QNS01": "Queens College",
    "QCC01": "Queensborough CC",
    "SOJ01": "School of Journalism",
    "SLU01": "School of Labor&Urban Studies",
    "LAW01": "School of Law",
    "MED01": "School of Medicine",
    "SPS01": "School of Professional Studies",
    "SPH01": "School of Public Health",
    "YRK01": "York College",
};

var sessions = {
    '1206': '2020 Summer Term',
    '1202': '2020 Spring Term',
    '1202_WIN': '2020 Winter Session (Spring Term)',
    '1199': '2019 Fall Term',
}

var activeCampus = null;

function buildCampusSwitchModal() {
    var s = "<select id='campus-option'>";
    for (var campus in campuses) {
        s += `<option value=${campus}>${campuses[campus]}</option>`;
    }
    s += "</select>";

    return `
<div class="modal" id="campus-switch-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Set active college</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>This will be the preset value of class searches. You can still search courses from other locations in the Search and Schedule tools.</p>
          <p>
          ${s}
          </p>
        </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick='onCampusSelect()'>OK</button>
        </div>
      </div>
    </div>
  </div>`
}

// var campusChangeEvent = document.createEvent('Event');

function onCampusSelect() {
    var val = $('#campus-option').val();
    var name = campuses[val];
    // alert('chose ' + val + name);
    $('#bar-location').text(name + ' ');

    // $('#course-search-modal').remove();

    // campusChangeEvent.initEvent('campusChangeEvent', true, true);
    // campusChangeEvent.campus = val;
    // var campusChangeEvent = new CustomEvent('campusChangeEvent', { campus: val });
    // document.dispatchEvent(campusChangeEvent);

    activeCampus = name;

    $(window).trigger('campusChangeEvent', { campus: val });
}

function buildSessionModal() {
    var v = `<select name="term_value" id="term-option" class="form-control">
    <option value="1202">2020 Spring Term</option>
    <option value="1206" disabled>2020 Summer Term</option>
    <option value="1202_WIN" disabled>2020 Winter Session (Spring Term)</option>
    <option value="1199" disabled>2019 Fall Term</option>
   </select>`;

    return `
   <div class="modal" id="session-switch-modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title">Set active term</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <p>Active session<br/><em>(** Currently only supports Spring 2020)</em></p>
         <p>
         ${v}
         </p>
       </p>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-primary" data-dismiss="modal" onclick='onSessionSelect()'>OK</button>
       </div>
     </div>
   </div>
 </div>
   `
}

function onSessionSelect() {
    var val = $('#term-option').val();
    var name = sessions[val];
    // alert('chose ' + val + name);
    $('#bar-session').text(name + ' ');

    // $('#course-search-modal').remove();

    // campusChangeEvent.initEvent('campusChangeEvent', true, true);
    // campusChangeEvent.campus = val;
    // var campusChangeEvent = new CustomEvent('campusChangeEvent', { campus: val });
    // document.dispatchEvent(campusChangeEvent);

    $(window).trigger('campusChangeEvent', { campus: val });
}

function changeActiveCollege() {
    if (!$('#campus-switch-modal').length) {
        $('body').append(buildCampusSwitchModal());
        $('#campus-switch-modal').modal('show');
    } else {
        $('#campus-switch-modal').modal('show');
    }
}

function changeSession() {
    if (!$('#session-switch-modal').length) {
        $('body').append(buildSessionModal());
        $('#session-switch-modal').modal('show');
    } else {
        $('#session-switch-modal').modal('show');
    }
}