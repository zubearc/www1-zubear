// document.addEventListener('DOMContentLoaded', function() {
//     var calendarEl = document.getElementById('calendar');

//     var calendar = new FullCalendar.Calendar(calendarEl, {
//       plugins: [ 'resourceTimeGrid' ],
//       defaultView: 'resourceTimeGridWeek',
//       resources: [

//       ]
//     });

//     calendar.render();
//   });

var events = [
    // {
    //     title:"My repeating event",
    //     start: '10:00', // a start time (10am in this example)
    //     end: '14:00', // an end time (2pm in this example)
    //     dow: [ 1, 4 ] // Repeat monday and thursday
    // }

];

function loadCalendar(events) {
    $('#calendar').fullCalendar({
        defaultView: 'agendaWeek',
        events: [
            // events go here
            //   {"title":"Conference","start":"2020-01-30","end":"2020-02-01"},{"title":"Meeting","start":"2020-01-31T10:30:00+00:00","end":"2020-01-31T12:30:00+00:00"},{"title":"Lunch","start":"2020-01-31T12:00:00+00:00"},{"title":"Birthday Party","start":"2020-02-01T07:00:00+00:00"},{"url":"http:\/\/google.com\/","title":"Click for Google","start":"2020-01-28"}
        ],
        selectable: true,
        height: 'parent',
        select: function (start, end, jsEvent, view) {
            // var abc = prompt('Enter Title');
            // var allDay = !start.hasTime && !end.hasTime;
            // var newEvent = new Object();
            // newEvent.title = abc;
            // newEvent.start = moment(start).format();
            // newEvent.allDay = false;
            // $('#calendar').fullCalendar('renderEvent', newEvent);

        },
        eventClick: function (calEvent, jsEvent, view) {

            alert('Event: ' + calEvent.title);
            // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            // alert('View: ' + view.name);

            // // change the border color just for fun
            // $(this).css('border-color', 'red');

        },
        resources: [
            //   { id: 'a', title: 'Room A' },
            //   { id: 'b', title: 'Room B' },
            //   { id: 'c', title: 'Room C' },
            //   { id: 'd', title: 'Room D' }
        ]
    });
}

function renderEvents(events) {
    $('#calendar').fullCalendar('renderEvents', events);
}

function clearCalendar() {
    $('#calendar').fullCalendar('removeEvents');
}

function updateCalendar(events) {
    $('#calendar').fullCalendar('removeEvents');
    $('#calendar').fullCalendar('addEventSource', events, true);
}

function addCalendarEvent(event) {
    $('#calendar').fullCalendar('addEventSource', event, true);
}

function getContrastYIQ(hexcolor){
    hexcolor = hexcolor.replace("#", "");
    var r = parseInt(hexcolor.substr(0,2),16);
    var g = parseInt(hexcolor.substr(2,2),16);
    var b = parseInt(hexcolor.substr(4,2),16);
    var yiq = ((r*299)+(g*587)+(b*114))/1000;
    return (yiq >= 128) ? 'black' : 'white';
}

function addEvent(title, string, color) {
    // Mo 8:50PM - 10:30PM
    let s = string.split(' ');
    var days = [];
    var hours = [];
    for (var e of s) {
        var dows = ['Sa', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Su'];
        for (var i in dows) {
            let dow = dows[i];
            if (e.includes(dow)) {
                days.push(parseInt(i));
            }
        }

        if (e.includes('AM')) {
            hours.push(e.replace('AM', ''));
        } else if (e.includes('PM')) {
            let [hr, min] = e.replace('PM', '').split(':');
            if (hr == '12')
                hours.push(e.replace('PM', ''));
            else
                hours.push((12 + parseInt(hr)) + ':' + min);
        }
    }
    console.log(days);
    console.log(hours);

    var t = { title: title, start: hours[0], end: hours[1], dow: days, color: color, textColor: getContrastYIQ(color) };
    console.log(t)
    addCalendarEvent([t]);
    // renderEvents([t]);
}

loadCalendar();
// updateCalendar(events);
// addEvent('Sample', 'MoWe 8:50PM - 10:30PM');

var stringToColour = function (str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

var ClassList = null;
var CourseList = null;

function loadCollege(collegePath) {
    $.getJSON(`serve/${collegePath}/ClassList.json`, (data) => {
        ClassList = data;
        $.getJSON(`serve/${collegePath}/CourseList.json`, (data2) => {
            CourseList = data2;
            // showCourse('CMP168');
        })
    })    
}

// loadCollege('lehman');

$(window).on("campusChangeEvent", (event, params) => {
    console.warn('campusChangeEvent', params);

    var val = params.campus;

    $('#course-search-modal').remove();

    if (val == 'LEH01') {
        loadCollege('lehman');
    } else if (val == 'JJC01') {
        loadCollege('jjay');
    }
});

function showCourse(course) {
    console.warn(course)
    var classesForCourse = CourseList[course];
    console.log(classesForCourse)
    for (var i in classesForCourse) {
        if (i == '0') continue;
        var clasId = classesForCourse[i];
        // console.log(clasForCourse)
        let clas = ClassList[clasId];
        console.log(clas);
        var times = clas[3].split('<br>');
        for (var time of times)
            addEvent(clas[0] + '-' + clas[2], time, stringToColour(clas.toString()));
    }
}

function buildCourseSearchModal() {
    var s = "<select id='course-picker' style='width:100%'>";
    for (var course in CourseList) {
        let name = CourseList[course][0];
        s += `<option value=${course}>${course} ${name}</option>`;
    }
    s += "</select>";

    return `
<div class="modal" id="course-search-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Search course</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>${CourseList == null ? "<b>Select an active campus before you can search</b>": "Searching courses from " + activeCampus}</p>
          <p>
          ${s}
          </p>
        </p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick='onCourseSelect()'>Show</button>
        </div>
      </div>
    </div>
  </div>`
}

function onCourseSelect() {
    clearCalendar();
    showCourse($('#course-picker').val());
}

function showCourseSearchModal() {
    if (!$('#course-search-modal').length) {
        $('body').append(buildCourseSearchModal());
        $('#course-picker').select2();
        $('#course-search-modal').modal('show');
    } else {
        $('#course-search-modal').modal('show');
    }
}