import re
import json

with open('fall2020_lehman.html') as f:
    contents = f.read()

_tables = contents.split('<table')

tables = _tables[8:-1]

classTable = {}
courseTable = {}
timesTable = {}

i = 0
while (i + 0) < len(tables):
    header = tables[i].replace('&nbsp;', '')
    # print header
    
    course_title = re.search("<\/a>(.*)<\/span>", header).group(1)
    try:
        course_id, course_name = course_title.split('-', 1)
    except Exception:
        print header

    print course_id, course_name

    body = tables[i + 1].replace('&nbsp;', '')
    # print body

    htmlentries = body.split('<tr')[2:]
    entries = [course_name]
    
    for htmlentry in htmlentries:
        htmlcol = re.findall(">(.*)<\/td>", htmlentry)
        # print htmlentry, htmlcol, len(htmlcol)
        _, Class, Section, DaysTimes, Room, Instructor, Mode, MeetingDates, Status, Topic = htmlcol
        # print Class, Section, DaysTimes, Room, Instructor, Mode, MeetingDates, Status, Topic
        
        class_id = re.search(">(.*)<", Class).group(1)
        section_id = re.search(">(.*)<", Section).group(1)
        status = re.search(' \"([a-zA-Z]+)\"', Status).group(1)
        x = [course_id, class_id, section_id, DaysTimes, Room, Instructor, Mode, MeetingDates, status, Topic]

        # print Class
        classTable[class_id] = x
        # print class_id
        entries.append(class_id)

        for dow in ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']:
            if dow not in timesTable:
                timesTable[dow] = []
            timesTable[dow].append(class_id)


    # entries = entries
    # print entries

    courseTable[course_id] = entries

    i += 2

j = json.dumps(classTable)
with open('ClassList.json', 'w') as f:
    f.write(j)

j2 = json.dumps(courseTable)
with open('CourseList.json', 'w') as f:
    f.write(j2)

j3 = json.dumps(timesTable)
with open('TimesCourse.json', 'w') as f:
    f.write(j3)