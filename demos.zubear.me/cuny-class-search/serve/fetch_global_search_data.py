import urllib2
import re

URL_GLOBAL_SEARCH = 'https://globalsearch.cuny.edu/CFGlobalSearchTool/search.jsp'

def get_global_data():
    request_headers = {
        "Accept-Language": "en-US,en;q=0.5",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0",
    }
    request = urllib2.Request(URL_GLOBAL_SEARCH, headers=request_headers)
    response = urllib2.urlopen(request).read()


    campuses_raw = re.findall(r"<label for='([A-Z0-9]+)'>&nbsp;([A-Za-z0-9& ]+)<\/label>", response)
    sessions_raw = re.findall(r"<option value='([A-Z0-9]+)'>([A-Za-z0-9& ]+)<\/option>", response)

    print(campuses_raw, sessions_raw)

print get_global_data()
