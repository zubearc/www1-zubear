var qLyrics = [];

async function qLyricsStart(currentms, gColor) {
    // var I = 0;

    var idelay = qLyrics[0].msdelay - currentms;

    console.warn(idelay);

    var correction = 0;

    if (idelay < 0) {
        correction = idelay * -1;
    } else {
        await delay(idelay);
    }

    for (var I = 0; I < qLyrics.length; I++) {
        let lyric = qLyrics[I];
        var starttime = Date.now();
        if (lyric.msdelay == null) continue;
        // console.log(lyric)

        let display_time = lyric.mscompletion - correction;
        console.log('cd', correction, display_time);

        console.log(lyric.lyric);
        // setTimeout(() => {

        let words = lyric.lyric.split(' ');
        let style = '';
        if (lyric.lyric.length >= 40) {
            style = 'font-size: 10vw!important;';
        } else if (lyric.lyric.length >= 50) {
            style = 'font-size: 8vw!important;';
        }

        console.log(display_time);

        write(qLyrics[0]);
        await showHiliTimed(lyric.lyric, 'green', gColor, display_time, 8);

        // let animation = 'fadeIn';
        // let animation = 'bounceInLeft';
        // $(".container").html(`<div class='playtext animated ${animation}' style='${style}'>${lyric.lyric}</div>`)
        // write(lyric.lyric);
        // let awaitTime = 0;
        // if (words.length < 2) { // blank or elrc
        //     await timeout(display_time);
        // } else {
        //     for (var i = 0; i < words.length; i++) {
        //         let txt = "";
        //         for (var j = 0; j < words.length; j++) {
        //             let _txt = words[j];
        //             txt += j <= i ? `<span style="color:${gColor};">${_txt}</span> ` : _txt + ' ';
        //         }
        //         $(".playtext").html(`${txt}`)
        //         console.info('Would await for ', Math.max(10, display_time) / (words.length - 1));
        //         await timeout(Math.max(10, display_time) / (words.length - 1));
        //         awaitTime += Math.max(10, display_time) / (words.length);
        //     }
        //     console.info(awaitTime);
        // }

        // $(".container").html(`<div class='playtext animated bounceInLeft'>${lyric.lyric}</div>`)

        // $("#playtext").text(lyric.lyric);
        // $("#playtext").removeClass("animated").addClass("animated");
        // });


        if (display_time < 0) {
            // correction = Math.abs(correction);
            // console.log('nc', correction);
            // await timeout(10);
        } else {
            console.log('sleep for', display_time);
            // await timeout(display_time);
        }

        var curtime = Date.now();

        if (display_time < 0) {
            // late_correction = ((curtime - starttime) - lyric.mscompletion) + (display_time*-1); // Time correction
            correction = ((curtime - starttime) - 10) + (display_time * -1);
        } else {
            correction = ((curtime - starttime) - display_time);
            console.log('correction', correction);
        }

        // I++;
    }
}

function loadLyrics(id, cb, fail) {
    $.get(`../lyrics/lyrics_${id}.txt`, (data) => {
        console.log(data);

        let d = data.split("\n");
        for (var i = 0; i < d.length; i++) {
            let line = d[i];
            var s = line.split('$');

            var mscompletion = 3000;

            if (d[i + 1]) {
                var _s = d[i + 1].split('$');
                mscompletion = _s[0];
            }

            if (s.length != 2) continue;
            qLyrics.push({ lyric: s[1], msdelay: parseInt(s[0]), mscompletion: parseInt(mscompletion) });

        }

        for (var line of data.split("\n")) {
            // console.log(line);
            var s = line.split('$');
            if (s.length != 2) continue;
            qLyrics.push({ lyric: s[1], msdelay: parseInt(s[0]) });
        }

        console.log(qLyrics);
        cb();
    });
}


//

function qLyricsInit() {
    var running = false;
    var playing = "";

    var ws = new WebSocket("ws://192.168.1.9:8083");
    ws.onmessage = function (data) {
        console.log(data.data);
        let p = JSON.parse(data.data);
        if (playing != p.id) {
            if (playing) {
                document.location.reload();
            }
            $('#status').text(p.id + ': ' + p.name);
            running = true;
            playing = p.id;
            try {
                var img = p.img[0].url;
                var img_height = p.img[0].height;
                var img_width = p.img[0].width;
                var name = p.name;
                var author = p.author;

                $('.container').html(`<div style="text-align:center;">
            <div class="art-container" style="object-fit: cover;">
            <img class="art" src="${img}" width=${img_width} height=${img_height}>
            </div>
                <h1>${name}</h1>
                <h1>${author}</h1>
            </div>`);
                console.log('done')

                var color = "white";
                if (p.id == "60wwxj6Dd9NJlirf84wr2c" || p.id == "1FlAMUpKrjY4NdaqsXjl1w" || p.id == '1Jj0GvrQqwRrcv0VSLiusw') {
                    color = "magenta";
                    console.warn('match!')
                    // document.body.style.backgroundColor = '#1b0016';
                    document.body.style.backgroundImage = 'url(https://i.scdn.co/image/c25a25b87dc2f1e9859b06363b1c02b907facab5)';
                    document.body.style.backgroundSize = 'cover';
                    // document.body.style.backgroundImage = 'url(https://wallpaperstock.net/sunset-clouds--dark-ocean-wallpapers_41023_2560x1440.jpg)';
                    document.body.style.color = '#92aac1';
                } else if (p.id == "3VKDOXkmTHt4Xa78U4dbDp") {
                    color = "orange";
                } else if (p.id == '0PiRp1VzEBHp68wY1M7zMs') {
                    color = '#e49a9a';
                    document.body.style.backgroundColor = 'rgb(10, 10, 12)';
                    document.body.style.backgroundImage = 'url(https://wallpaperstock.net/sunset-clouds--dark-ocean-wallpapers_41023_2560x1440.jpg)';
                    document.body.style.color = 'darkgray';
                    // document.body.style.backgroundColor = 'rgb(195, 163, 163)';
                } else if (p.id == '1a0Mg4XPdt4EKsUz5beRZY' || p.id == '3JPXFD2aJ4dHPMnGwo41bO' || p.id == '1aFvsXY4iv75uyW54kwDly') {
                    document.body.style.backgroundImage = 'url(https://i.scdn.co/image/250f37e935fcb95ef05c5b0951593f16c788b41e)';
                    // https://i.scdn.co/image/66c41643f387302f44d24202801640eaab275a94
                    document.body.style.backgroundSize = 'cover';
                    color = "orange";
                } else {
                    console.warn('fail', p.id)
                    // document.body.style.backgroundImage = 'url(' + img + ')';
                    // document.body.style.backgroundSize = 'cover';
                }

                loadLyrics(p.id, () => {
                    qLyricsStart(p.cp, color);
                });
            } catch (e) {
                running = false;
            }
        }
    }
}