var pixelSize = null;

// //grid width and height
var bw = pixelSize * 16;
var bh = pixelSize * 8;
// //padding around grid
var p = 0;
//size of canvas
var cw = bw + (p*2) + 1;
var ch = bh + (p*2) + 1;


// var canvas = $('<canvas/>').attr({width: cw, height: ch}).appendTo('body');

var context = null;
var canvas = null;

function drawBoard(){
    for (var x = 0; x <= bw; x += pixelSize) {
        context.moveTo(0.5 + x + p, p);
        context.lineTo(0.5 + x + p, bh + p);
    }


    for (var x = 0; x <= bh; x += pixelSize) {
        context.moveTo(p, 0.5 + x + p);
        context.lineTo(bw + p, 0.5 + x + p);
    }

    context.strokeStyle = "black";
    context.stroke();
}

function drawPixel(x, y, color) {
/* 	context.beginPath();
	context.rect(0, 0, pixelSize, pixelSize);
	context.fillStyle = 'red';
	context.fill(); */

	context.beginPath();
	context.rect(pixelSize * x, pixelSize * y, pixelSize, pixelSize);
	context.fillStyle = color||'green';
	context.fill();
}

function clearBoard() {
	context.clearRect(0, 0, cw, ch);
	drawBoard();
}

function random(bottom, top) {
	return Math.floor( Math.random() * ( 1 + top - bottom ) ) + bottom;
}

function initBoard(pixelSize, width, height) {
    window.pixelSize = pixelSize;
    window.bw = pixelSize * width;
    window.bh = pixelSize * height;

    window.cw = bw + (p*2) + 1;
    window.ch = bh + (p*2) + 1;

    document.getElementById('matrice-holder').innerHTML = `<canvas id='canvas' width=${cw} height=${ch} />`;
    window.canvas = document.getElementById('canvas');
    window.context = canvas.getContext("2d");
}
// initBoard(40, 32, 8);

// drawBoard();
// /* drawPixel(0,0); */
// /* clearBoard(); */
// /* drawBoard(); */
// setInterval(() => {
//   clearBoard();

// 	let x = random(0, 15);
//   let y = random(0, 7);
//   drawPixel(x,y);
// }, 100);