var table = [];

function draw(x, y, color) {
    table[y][x] = color || 'green';
    // drawPixel(x, y, color);
}

var useNewFonts = true;

function drawChar(xoff, yoff, char, style, dry) {
    if (useNewFonts) {
        var pm = D[char] || C[char];
    } else {
        var pm = C[char];
    }
    if (!pm) {
        return 0; // dont have pixel map for char, spaces
    }

    let cwidth = 0;

    for (var i in pm) {
        let pix = pm[i];
        cwidth = Math.max(cwidth, pix[1] + 1);
        let x = pix[1] + xoff;
        let y = pix[0] + yoff;
        if (x >= table_dims[1] || y >= table_dims[0] || x < 0 || y < 0) continue;
        if (!dry) {
            table[y][x] = style || 'green';
        }
    }

    return cwidth;
}

function writeDry(text) {
    let x = 0;
    let lwlen = 0;
    for (var i in text) {
        let c = text[i];
        let s = null;
        lwlen = drawChar(x, 0, c, '', true);
        x += lwlen + 1;
    }
    return x;
}

function write(text, style, startingIndex) {
    var x = startingIndex || 0;

    if (x == 0xff) { // center
        var len = writeDry(text);
        let dif = TABLE_WIDTH - len;
        x = Math.floor(dif / 2);
    }

    let lwlen = 0;
    var readingEscape = true;
    for (var i = 0; i < text.length; i++) {
        let c = text[i];
        if (c == '~') {
            c = `.${text[i + 1]}${text[i + 2]}${text[i + 3]}`;
            i += 4;
        }
        let s = null;
        lwlen = drawChar(x, 0, c, style);
        x += lwlen + 1;
    }
    return x;
}



function genPixMap() { // init/flush
    let rows = table_dims[1]
    let cols = table_dims[0]

    for (var i = 0; i < cols; i++) {
        var col = [];
        for (var j = 0; j < rows; j++) {
            col.push(null);
        }

        table.push(col);
    }
}

function flush() {
    for (var i = 0; i < table.length; i++) {
        var cval = table[i];
        for (var j = 0; j < cval.length; j++) {
            cval[j] = null;
        }
    }
}

function render() {
    // preRender(preRenderP1, preRenderP2);

    clearBoard();
    for (var i = 0; i < table.length; i++) {
        var cval = table[i];
        for (var j = 0; j < cval.length; j++) {
            if (cval[j]) {
                drawPixel(j,i,cval[j]);
            }
            // col.push(cval[j] || 'background-color: lightgray;');
        }
    }
}


//

function write(text, style, X, Y) {
    var x = X || 0;
    let y = Y || 0;

    // if (x == 0xff) { // center
    //     var len = writeDry(text);
    //     let dif = TABLE_WIDTH - len;
    //     x = Math.floor(dif / 2);
    // }

    let lwlen = 0;
    var readingEscape = true;
    for (var i = 0; i < text.length; i++) {
        let c = text[i];
        if (c == '~') {
            c = `.${text[i + 1]}${text[i + 2]}${text[i + 3]}`;
            i += 4;
        }
        let s = null;
        lwlen = drawChar(x, y, c, style);
        x += lwlen + 1;
    }
    return x;
}

function writeDry(text) {
    let x = 0;
    let lwlen = 0;
    for (var i in text) {
        let c = text[i];
        let s = null;
        lwlen = drawChar(x, 0, c, '', true);
        x += lwlen + 1;
    }
    return x;
}

function _showScrolling(text, style, speed, startingIndex, until) {
    let ti = startingIndex || 0;
    until = until | text.length;

    var speed = speed || 100;
    let took = 0;

    var timer = setInterval(() => {
        flush();
        write(text, style, ti);
        ti--;
        console.log(ti,until,-TABLE_WIDTH);
        if ((ti) == -until + 32) {
            clearInterval(timer);
            // console.log('done', took, text.length);
        }
        render();
        took += speed;
    }, speed);
    return speed * Math.abs(TABLE_WIDTH - until);
}

function showScrollable(text,style,speed) {
    var writable = write(text);
    if (writable > TABLE_WIDTH) {
        return _showScrolling(text, style, speed, null, writable);
    } else {
        render();
    }
}


///

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  
async function renderScrollingHighlight(text, lowerColor, upperColor, speed, y) {
    var len = writeDry(text);
    var xoff = 0;
    y = y || 0;

    var dif = TABLE_WIDTH - len;
	xoff = Math.floor(dif / 2);

    // var didOverdraw = false;
    // int oldWidth = globalWindow.width;
    // int oldHeight = globalWindow.height;

    if (len > TABLE_WIDTH) {
        xoff = 0;

        // if (allowOverdraw) {
        //     wRestoreWriteRegion();
        //     len = dryWrite(text, font);
        //     if (len > PIXEL_COLUMNS) {
        //         font = FontType::Old;
        //     }
        //     didOverdraw = true;
        // } else {
        //     font = FontType::Old;
        // }
    } else {
        // fprintf(stderr, "PIXEL_COLUMNS(%d) - len(%d)=%d, floor(/ 2) = %d\n", PIXEL_COLUMNS, len, dif, xoff);
    }

    var wait_time = speed / text.length;

    for (var i = 0; i < text.length; i++) {
        flush();
        var _c = text[i];

        if (upperColor == 0xf0f0) {
            var red = random(0x10, 100);
            var green = random(0x10, 100);
            var blue = random(0x10, 100);
            upperColor = pack(0x10, blue, green, red);
        }

        var x = xoff;
        var lwlen = 0;
        for (var j = 0; j < text.length; j++) {
            var c = text[j];
            lwlen = drawChar(x, y, c, i >= j ? upperColor : lowerColor);
            x += lwlen + 1;
        }

        render();
        await delay(wait_time);
    }

    return;
}

async function showHiliTimed(text, primaryColor, color, completeWithinMS, y) {
    var tempTime = 0;

	var count = text.split(' ').length;
	// if (count == 0) {
	// 	return writeFlashing(text, color);
	// }
	var speed = Math.abs(completeWithinMS / count);
	// printf("%d / %d -> Speed: %4.4f\n", completeWithinMS, count, speed);

    var pos = 0;
    var s;

    for (var s of text.split(' ')) {
        flush();
		// Serial.println(s);

//
        // renderScrollingHighlight(s, 0x20, 0x2000F0, speed);
        await renderScrollingHighlight(s, primaryColor, color, speed, y);
        continue;
//

        // auto len = write(s, color);

        // if (len > PIXEL_COLUMNS) {
        //     flush();
        //     write(s, color, FontType::Old);
        //     // render();
        //     // delay(400);
        //     // renderScrolling(s, s.length(), color, len, 100);
        // }

        // render();
        // // if (len < 30)
        //     // delay(speed - 100);
        // // else
        //     delay(speed);
    }
    // flush();
    // render();

    // auto timer = nullptr;
}