var TABLE_HEIGHT = 8;
var TABLE_WIDTH = 64;

var table_dims = [TABLE_HEIGHT, TABLE_WIDTH];

var w = QueryString.getValue("width");
if (w) {
    table_dims[1] = parseInt(w);
    TABLE_WIDTH = parseInt(w);
}

var w = QueryString.getValue("height");
if (w) {
    table_dims[0] = parseInt(w);
    TABLE_HEIGHT = parseInt(w);
}

var pxSize = QueryString.getValue("size") || 40;

initBoard(parseInt(pxSize), TABLE_WIDTH, TABLE_HEIGHT);
genPixMap();
// draw(0, 1, 'red');
render();

drawBoard();

// let id = '1a0Mg4XPdt4EKsUz5beRZY';
// loadLyrics(id, () => {
//     qLyricsStart(0, 'red');
// });

qLyricsInit();