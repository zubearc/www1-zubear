console.debug=() => {};
function processScreenBuffer(arr, len) {
    let buffer = new Uint32Array(arr);
    console.debug(buffer);
    // for (var i = 0; i < len; i++) {
    //     if (buffer[i] != 0) {
    //         console.debug('i', i, buffer[i]);
    //         ws2128bToPixelMap(i);
    //     }

    // }

    flush();
    for (var x = 0; x < 64; x++) {
        for (var y = 0; y < 8; y++) {
            // var i = (y * 64) + x;
            // var V = buffer[i];
            // if (V) {
            //     console.debug('col', x, y, buffer[i]);
            //     // ws2128bToPixelMap(y * 64 + x);
            // }
            // draw(x, y, V != 0?'background-color:red;':null);
            var i = pixelMapToWs2128b(x,y);
            // // buffer[i];
            if (buffer[i]) {
                let pix = buffer[i];
                let red = pix & 0xff;
                let green = (pix >> 8) & 0xff;
                let blue = (pix >> 16) & 0xff;
                // console.debug('Red:',pix&0xff);
            //     console.debug('col', x, y, buffer[i]);
            //     ws2128bToPixelMap(i);
                draw(x,y,buffer[i] != 0 ? `background-color:rgb(${red}, ${green}, ${blue});` : null);
            }
        }
    }
    render();
}

function processBinaryUpdates(data) {
    var version_view = new DataView(data, 0, 2);
    
    let version = version_view.getUint16(0, true);

    var current_index = 4;

    var readMessage = function() {
        console.debug('READING MESSAGE, CURR INDEX=%d', current_index)
        var header_view = new DataView(data, current_index, 8);
        let message_len = header_view.getInt32(0, true);
        if (message_len == 0)
            return;
        //console.assert(message_len > 4, 'message_len > 4');
        let message_type = header_view.getInt32(4, true);
        current_index += 8;

        console.debug('-> FB version=%d, len=%d, type=%d', version, message_len, message_type);

        console.debug(data, current_index, message_len, 2058 - current_index);
        // var message = new Uint8Array(data, current_index, message_len);
        var message = new Uint32Array(data, current_index, message_len / 4);
        current_index += message_len;

        switch (message_type) {
            case 4:
                processScreenBuffer(message, message_len);
                break;
            default:
            console.error('unknown packet type %d', message_type)
        }    
    }
 
    var max_messages = 256;
    // data is LE so we can read first byte -- messages usually never above 256 bytes (for now)
    while ((current_index + 4) < data.byteLength) { // still have data to read
        readMessage();
        if (max_messages-- == 0) {
            break;
        }
    }
    console.debug('Current Index: %d -- Byte Length: %d', current_index, data.byteLength)

    // var message = new Uint8Array(data, 10, message_len - 4);
    // var message_buf = new flatbuffers.ByteBuffer(message);
    
}

var ws = new WebSocket("ws://192.168.1.81:6761/");

function onMessage(event) {
    if (event.data instanceof ArrayBuffer) {
        // binary data, read as flatbuffer
        console.debug(event.data)
        // console.debug("-> BIN %d bytes", event.data.length);
        processBinaryUpdates(event.data);
    } else {
        // booooring text data :)
        let j = JSON.parse(event.data);
        console.debug('got JSON', j);
        // if (j.t == 4) {
        //    processServerUpdates(j.updates);
        // }
    }
}
var embedded = false;
ws.onclose = function(event) {
    if (embedded) {
        setTimeout(function() {
            document.location.reload();
        }, 4000);
        return;
    }
        
    var should_reconnect = confirm("Lost connection to the server. Would you like to reconnect?")
    if (should_reconnect) {
        document.location.reload();
    }
}

ws.binaryType = 'arraybuffer';
ws.onmessage = onMessage;
