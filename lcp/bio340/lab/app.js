function build() {

}

var gData = {};
var gGridSize = 3;

var lastModule = 2;

var driveUrls = {
    '2': 'https://drive.google.com/open?id=1ve994qYBzoD_VM2c5HcZlh3VnsbgRD99',
    '3': 'https://drive.google.com/open?id=1w9ep8XDE2K5QgYLjbLIW7krl1GJ9Dnoq',
    '4': 'https://drive.google.com/open?id=1KzQvGyWNRG4cAGvdsqmQ-Hzg1rj9dnFS',
    '5': 'https://drive.google.com/open?id=188CoqWoPggmq48fTaUt_opiUxnIRjhKS',
    '6': 'https://drive.google.com/open?id=1BCXnokCDE6v0dNb7mIW03UbtVFWXf8Bs',
    '7': 'https://drive.google.com/open?id=119P68avsQDLgdn38Cqf1q9s5icn5ZyGI',
    '8': 'https://drive.google.com/open?id=1Ymq6mB4jgdIuP252vmQDjK4vJ_GsyQoY'
}

var host = (location.href.includes('192') || location.href.includes('localhost')) ? '' : 'https://realtimenyc-staging.s3.amazonaws.com/bio341/';

function buildModule(module) {

    if (!module) {
        module = lastModule;
    }

    lastModule = module;

    let entries = gData[module];

    console.log('build', module, entries)

    if (!entries) {
        console.warn('No entries for module', module);
        return;
    }

    var html = '';


    if (gGridSize == 3) {
        html += '<div class="row">';
        for (var i = 0; i < entries.length;) {
            var one = entries[i];
            var two = entries[i + 1];
            var three = entries[i + 2];

            var he = '';

            function addEntry(e) {
                if (e)
                    he += `<div class="col-lg-4 col-md-4 col-xs-4 thumb">
                            <a class="thumbnail" href="module${module}/${e[0]}" target="_blank">
                                <img src="${host}module${module}/${e[0]}" />
                            </a>
        
                            <br />
                            <p style='text-align: center; font-weight: bold;padding:4px; font-size: large;'>${e[1]}</p>
                        </div>`
            }

            addEntry(one);
            addEntry(two);
            addEntry(three);

            html += he;

            i+= 3;
        }
        html += '</div>'
    } else if (gGridSize == 1) {
        var he = '';

        for (var e of entries) {
            he += `<div>
            <div>
            <a class="thumbnail" href="module${module}/${e[0]}" target="_blank">
            <img src="${host}module${module}/${e[0]}" />
        </a>
                <br />
                <p style='text-align: center; font-weight: bold;padding:4px; font-size: large;'>${e[1]}</p>
            </div>
        </div>`
        }

        html += he;
    }

    console.log(html)

    $('#m-pictures').html(html);

    $('#m-extra').html('');
    $.get('component-module'+module+'.html', function(data) {
        $('#m-extra').html(data);
    })
}

function showModule(module) {
    buildModule(module);
    $('.card').removeClass('card-active');
    $('#m' + module).addClass('card-active');
}

function setGridSize(size) {
    gGridSize = size;
    showModule(lastModule);
}

$.getJSON('data.json?5', function (data) {
    gData = data;
    build();

    showModule(lastModule);
})

function openDriveLink() {
    openInNewTab(driveUrls[lastModule]);
}

function openInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
  }